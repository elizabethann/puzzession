var StatusLayer = cc.Layer.extend({
	labelGoo:null,
	labelMeter:null,
	gooGlobs: 0,

	ctor:function () {
		this._super();
		this.init();
	},
	updateMeter:function (px) {
		this.labelMeter.setString(parseInt(px / 10) + "M");
	},
	addGoo:function (num){
		this.gooGlobs += num;
		this.labelGoo.setString("GooGlobs: " + this.gooGlobs);
	},

	init:function(){
		this._super();

		var winsize = cc.director.getWinSize();

		this.labelGoo = new cc.LabelTTF("GooGlobs:0", "Helvetica", 20);
		this.labelGoo.setColor(cc.color(0, 0, 0)); //black
		this.labelGoo.setPosition(cc.p(70, winsize.height - 20));
		this.addChild(this.labelGoo);

		this.labelMeter = new cc.LabelTTF("0M", "Helvetica", 20);
		this.labelMeter.setPosition(cc.p(winsize.width -70, winsize.height - 20));
		this.addChild(this.labelMeter);
	}
});