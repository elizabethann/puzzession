var PlayScene = cc.Scene.extend({
	space:null,
	gameLayer:null,
	shapesToRemove:[],

	initPhysics:function() {
		this.space = new cp.Space();
		this.space.gravity = cp.v(0, -350);

		var wallBottom = new cp.SegmentShape(this.space.staticBody,
			cp.v(0, g_groundHeight),
			cp.v(4294967295, g_groundHeight),
			0);
		this.space.addStaticShape(wallBottom);

		// Handle collisions
		this.space.addCollisionHandler(SpriteTag.runner,
			SpriteTag.goo,
			this.collisionGooBegin.bind(this), 
			null, null, null);
		this.space.addCollisionHandler(SpriteTag.runner,
			SpriteTag.flash,
			this.collisionFlashBegin.bind(this), 
			null, null, null);
	},
	collisionGooBegin:function (arbiter, space){
		cc.audioEngine.playEffect(res.pickup_coin_mp3);
		var shapes = arbiter.getShapes();
		// shapes[0] == runner
		this.shapesToRemove.push(shapes[1]);

		// update goo count using collision
		var statusLayer = this.getChildByTag(TagOfLayer.Status);
		statusLayer.addGoo(1);
	},
	collisionFlashBegin:function (arbiter, space){
		cc.audioEngine.stopMusic();
		cc.log("==game over");
		cc.director.pause();
		this.addChild(new GameOverLayer());
	},
	update:function(dt){
		this.space.step(dt);

		var animationLayer = this.gameLayer.getChildByTag(TagOfLayer.Animation);
		var eyeX = animationLayer.getEyeX();

		this.gameLayer.setPosition(cc.p(-eyeX, 0));

		// delete unused bodies
		for(var i = 0; i < this.shapesToRemove.length; i++){
			var shape = this.shapesToRemove[i];
			this.gameLayer.getChildByTag(TagOfLayer.background).removeObjectByShape(shape);
		}
		this.shapesToRemove = [];
	},
	onEnter:function() {
		this.shapesToRemove = [];
		this._super();
		this.initPhysics();
		this.gameLayer = new cc.Layer();

		this.gameLayer.addChild(new BackgroundLayer(this.space), 0, TagOfLayer.background);
		this.gameLayer.addChild(new AnimationLayer(this.space), 0, TagOfLayer.Animation);
		this.addChild(this.gameLayer);
		this.addChild(new StatusLayer(), 0, TagOfLayer.Status);


		cc.audioEngine.playMusic(res.background_mp3, true);

		this.scheduleUpdate();

	}
});