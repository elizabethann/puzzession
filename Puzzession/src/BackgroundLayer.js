var BackgroundLayer = cc.Layer.extend({
	map00:null,
	map01:null,
	mapWidth:0,
	mapIndex:0,
	space:null,
	spriteSheet:null,
	objects:[],

	ctor:function (space) {
		this._super();

		this.objects = [];
		this.space = space; 

		this.init();
	},
	loadObjects:function (map, mapIndex) {
		var gooGroup = map.getObjectGroup("goo");
		var gooArray = gooGroup.getObjects();
		for (var i = 0; i < gooArray.length; i++){
			var goo = new Goo(this.spriteSheet,
				this.space,
				cc.p(gooArray[i]["x"] + this.mapWidth * mapIndex, gooArray[i]["y"]));
			goo.mapIndex = mapIndex;
			this.objects.push(goo);
		}

		var flashGroup = map.getObjectGroup("flash");
		var flashArray = flashGroup.getObjects();
		for (var i = 0; i < flashArray.length; i++){
			var flash = new Flashlight(this.spriteSheet,
				this.space,
				flashArray[i]["x"] + this.mapWidth * mapIndex);
			flash.mapIndex = mapIndex;
			this.objects.push(flash);
		}
	},
	checkAndReload:function (eyeX){
		var newMapIndex = parseInt(eyeX / this.mapWidth);
		if(this.mapIndex == newMapIndex){
			return false;
		}
		if (0 == newMapIndex % 2) {
			this.map01.setPositionX(this.mapWidth * (newMapIndex + 1));
			this.loadObjects(this.map01, newMapIndex + 1);
		} else {
			this.map00.setPositionX(this.mapWidth * (newMapIndex + 1));
			this.loadObjects(this.map00, newMapIndex + 1);
		}
		this.removeObjects(newMapIndex - 1);
		this.mapIndex = newMapIndex;
		return true;
	},
	removeObjects:function (mapIndex) {
		while((function (obj, index) {
            for (var i = 0; i < obj.length; i++) {
                if (obj[i].mapIndex == index) {
                    obj[i].removeFromParent();
                    obj.splice(i, 1);
                    return true;
                }
            }
            return false;
        })(this.objects, mapIndex));
	},
	removeObjectByShape:function (shape) {
        for (var i = 0; i < this.objects.length; i++) {
            if (this.objects[i].getShape() == shape) {
                this.objects[i].removeFromParent();
                this.objects.splice(i, 1);
                break;
            }
        }
    },
	update:function(dt) {
		var animationLayer = this.getParent().getChildByTag(TagOfLayer.Animation);
		var eyeX = animationLayer.getEyeX();
		this.checkAndReload(eyeX);
	},
	init:function() {
		this._super();
		this.map00 = new cc.TMXTiledMap(res.map00_tmx);
		this.addChild(this.map00);
		this.mapWidth = this.map00.getContentSize().width;
		this.map01 = new cc.TMXTiledMap(res.map01_tmx);
		this.map01.setPosition(cc.p(this.mapWidth, 0));
		this.addChild(this.map01);


		cc.spriteFrameCache.addSpriteFrames(res.background_plist);
		this.spriteSheet = new cc.SpriteBatchNode(res.background_png);
		this.addChild(this.spriteSheet);

		this.loadObjects(this.map00, 0);
		this.loadObjects(this.map01, 1);

		this.scheduleUpdate();


	}
});