var res = {
    helloBG_png : "res/hello_BG.png",
    start_N_png : "res/start_n.png",
    start_S_png : "res/start_s.png",
    playBG_png : "res/playBackground.png",
    runner_png : "res/running.png",
    runner_plist : "res/running.plist",
    map_png: "res/map.png",
    map00_tmx: "res/map00.tmx",
    map01_tmx: "res/map01.tmx",
    background_png: "res/background.png",
    background_plist: "res/background.plist",
    restart_n_png: "res/restart_n.png",
    restart_s_png: "res/restart_s.png",
    background_mp3: "res/backgroundMusic.mp3",
    jump_mp3: "res/jump.mp3",
    pickup_coin_mp3 : "res/pickup_coin_mp3"
};

var g_resources = [
	res.helloBG_png,
	res.start_N_png,
	res.start_S_png,
	res.playBG_png,
	res.runner_png,
	res.runner_plist,
	res.map_png,
	res.map00_tmx,
	res.map01_tmx,
	res.background_png,
	res.background_plist,
	res.restart_n_png,
	res.restart_s_png,
	res.background_mp3,
	res.jump_mp3,
	res.pickup_coin_mp3

];